@login
Feature: Login
  User should be able to login with valid username and password combination
  If user provides invalid username and/or password login should be denied.

  Background:
    User is already registered with e-mail address: himmerzsolt@gmail.com and password test123


  Scenario: Successful login
    Given User is on login page
    And user fills Email address textbox with himmerzsolt@gmail.com
    And user fills "Password" textbox with valid password
    When User clicks on sign in button
    Then Login must be successful


  Scenario Outline: Unsuccessful login with invalid e-mail and valid password
    Given User is on login page
    And user fills Email Address textbox with "<invalid e-mail>"
    And user fills "Password" textbox with valid password
    When User clicks on sign in button
    Then Login must be unsuccessful

    Examples:

      | invalid e-mail        |
      | test@gmail.com        |
      | wrongemail@gmail.com  |


  Scenario Outline: Unsuccessful login with valid e-mail and invalid password
    Given User is on login page
    And user fills Email address textbox with himmerzsolt@gmail.com
    And user fills "Password" textbox with "<invalid password>"
    When User clicks on sign in button
    Then Login must be unsuccessful

    Examples:

      | invalid password |
      | test             |
      | wrongpassword    |


    Scenario: Unsuccessful login with valid e-mail and empty password
      Given User is on login page
      And user fills Email address textbox with himmerzsolt@gmail.com
      And user leaves "Password" textbox empty
      When User clicks on sign in button
      Then password required error message appears


    Scenario: Unsuccessful login with empty email and valid password
      Given User is on login page
      And user leaves Email address textbox empty
      And user fills "Password" textbox with valid password
      When User clicks on sign in button
      Then email address required error message appears


