
@search-query
Feature: Search-query
  User should be able to search for products by entering text (product name, or part of the name of a product)
  in the search box. After clicking on submit search button user should see the search result.
  Search result contains only relevant products (where the image title contains the searched query)

  Background:
  User is already registered with e-mail address: himmerzsolt@gmail.com and password test123,
  however registration is not a pre-condition to use this feature


  Scenario Outline: Successful search for existing products
    Given user is already logged-in
    When user enters "<searched query>" in search box
    And clicks on submit search button
    Then user should see  products where image title contains "<searched query>"

    Examples:

      | searched query |
      | blouse       |
      | summer       |
      | t-shirt      |


  Scenario Outline: Unsuccessful search for non-existing products
    Given user is already logged-in
    When user enters "<searched query>" in search box
    And clicks on submit search button
    Then search result does not show any item

    Examples:

      | searched query |
      | winter       |


