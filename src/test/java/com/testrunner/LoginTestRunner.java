package com.testrunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


//@RunWith(Cucumber.class) - JUnit
@CucumberOptions(features="src/test/java/resources/mystore/login.feature" , glue = {"com.automationpractice"})
public class LoginTestRunner extends AbstractTestNGCucumberTests {
}
