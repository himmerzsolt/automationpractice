package com.testrunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/java/resources/mystore/search-query.feature" , glue = {"com.automationpractice"})
public class SearchQueryTestRunner extends AbstractTestNGCucumberTests {
}
