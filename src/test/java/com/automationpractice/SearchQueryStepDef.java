package com.automationpractice;

import com.automationpractice.pageobjects.BasePage;
import com.automationpractice.pageobjects.Landing;
import com.automationpractice.pageobjects.Login;
import com.automationpractice.util.CookieWrite;
import com.automationpractice.util.RunEnvironment;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class SearchQueryStepDef {
    private WebDriver driver;
    private Landing landing;
    private SoftAssert softAssert = new SoftAssert();

    @Before("@search-query")
    public void setup(){
        driver = RunEnvironment.getWebDriver();
        landing = new Landing(driver);
        landing.enterFullScreen();

    }

    @Given("user is already logged-in")
    public void  user_is_already_logged_in(){
        CookieWrite.writeCookie(driver);
    }
    @When("user enters {string} in search box")
    public void user_enters_search_query_in_search_box(String searchQuery){
        landing.typeTextInSearchField(searchQuery);
    }

    @And("clicks on submit search button")
    public void clicks_on_submit_search_button(){
        landing.clickOnSubmitSearch();
    }

    @Then("user should see  products where image title contains {string}")
    public void user_should_see_products_where_image_title_contains_searched_query(String searchedQuery){
        List<WebElement> searchResults=  landing.getSearchResults();

        for(int i=0; i<searchResults.size(); i++){
        softAssert.assertTrue(searchResults.get(i).getAttribute("title").toLowerCase().contains(searchedQuery), searchResults.get(i).getAttribute("title") + " does not contain searchedQuery: " + searchedQuery);
        }
        softAssert.assertAll();
    }

    @Then("search result does not show any item")
    public void search_result_does_not_show_any_item(){
        Assert.assertTrue(landing.getContentResultCounterText().contains("0 results have been found."), "There is at least 1 result");
    }

    @After("@search-query")
    public void quitBrowser(){
        landing.tearDown();
    }

}
