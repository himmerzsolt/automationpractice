package com.automationpractice;

import com.automationpractice.pageobjects.BasePage;
import com.automationpractice.pageobjects.Landing;
import com.automationpractice.pageobjects.Login;
import com.automationpractice.util.CookieRead;
import com.automationpractice.util.RunEnvironment;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class LoginStepDef {

    private WebDriver driver;
    private BasePage basePage;
    private Login login;
    private Landing landing;

    @Before("@login")
    public void setup(){
        driver = RunEnvironment.getWebDriver();
        basePage = new BasePage(driver);
        login = new Login(driver);
        landing = new Landing(driver);
    }

    @Given("User is on login page")
    public void user_is_on_login_page(){
        basePage.init();
        basePage.clickOnSignIn();

    }

    @And("user fills Email Address textbox with {string}")
    public void user_types_invalid_email(String email){
    login.typeEmailAddress(email);
    }


    @And("user fills Email address textbox with himmerzsolt@gmail.com")
    public void user_types_valid_email(){
        login.typeEmailAddress("himmerzsolt@gmail.com");
    }

    @And("user fills \"Password\" textbox with valid password")
    public void user_types_valid_pw(){
        login.typePassword("test123");
    }
    @And("user fills \"Password\" textbox with {string}")
    public void user_types_invalid_password(String password){
        login.typePassword(password);
    }

    @And("user leaves \"Password\" textbox empty")
    public void user_leaves_Password_textbox_empty(){

    }

    @And("user leaves Email address textbox empty")
    public void user_leaves_Email_address_textbox_empty(){

    }

    @When("User clicks on sign in button")
    public void user_clicks_on_sign_in_button(){
        login.clickOnSignInButton();
    }

    @Then("Login must be successful")
    public void login_must_be_successful(){
        Assert.assertEquals(landing.checkMyAccountButton(),"My account", "login was successful");
        CookieRead.getCookies(driver);
    }

    @Then("Login must be unsuccessful")
        public void login_must_be_unsuccessful(){
        Assert.assertEquals(login.checkErrorMessage(), "Authentication failed.", "error message is not the expected one!");
        }
    @Then("password required error message appears")
    public void password_required_error_message_appears(){
        Assert.assertEquals(login.checkErrorMessage(), ("Password is required."), "error message is not the expected one!");
    }

    @Then("email address required error message appears")
    public void email_address_required_error_message_appears(){
        Assert.assertEquals(login.checkErrorMessage(), ("An email address required."), "error message is not the expected one!");
    }
    @After("@login")
    public void quitBrowser(){
        basePage.tearDown();
    }

}
