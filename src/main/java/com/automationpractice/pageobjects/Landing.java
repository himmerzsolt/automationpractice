package com.automationpractice.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class Landing extends BasePage {

    @FindBy (css ="#columns > div.breadcrumb.clearfix > span.navigation_page")
    private WebElement myAccount;



    public Landing(WebDriver driver) {
        super(driver);
    }

    public String checkMyAccountButton(){
        return myAccount.getText();
    }

}
