package com.automationpractice.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login extends BasePage {

    @FindBy(css = "#email")
    private WebElement inputEmailAddress;

    @FindBy(css = "#passwd")
    private WebElement inputPassword;
    @FindBy(css = "#SubmitLogin > span > i")
    private WebElement submitLogin;

    @FindBy (css = "#center_column > div.alert.alert-danger > ol > li")
    private WebElement errorMessage;

    public Login (WebDriver driver){
        super(driver);
    }
    public void typeEmailAddress(String email){
        inputEmailAddress.sendKeys(email);
    }
    public void typePassword(String pw){
        inputPassword.sendKeys(pw);

    }
    public void clickOnSignInButton(){
        submitLogin.click();
    }
    public String checkErrorMessage(){
        return errorMessage.getText();
    }


}
