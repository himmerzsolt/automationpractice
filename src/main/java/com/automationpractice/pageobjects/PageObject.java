package com.automationpractice.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

//Every pageobject is supposed to extend  this Class
public class PageObject {
    WebDriver driver;


    PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }
    public void enterFullScreen(){
        driver.manage().window().maximize();
    }

    public void tearDown(){
        driver.quit();
    }

}
