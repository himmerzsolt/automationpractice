package com.automationpractice.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BasePage extends PageObject {

    private final String BASE_URL = "http://automationpractice.com/index.php";
    @FindBy(css ="#header > div.nav > div > div > nav > div.header_user_info > a")
    private WebElement signInButton;

    @FindBy(css = "#search_query_top")
    private WebElement searchInputField;

    @FindBy(css = "#searchbox > button")
    //@FindBy(css ="#searchbox  > .btn btn-default button-search")
    private WebElement submitSearch;

    @FindBy(css = "#center_column > h1 > span")
    private WebElement resultCounterText;


    public BasePage(WebDriver driver) {
        super(driver);
    }
    public void clickOnSignIn(){
        signInButton.click();
    }

    public void init() {
        this.driver.get(this.BASE_URL);
        driver.manage().window().maximize();
    }

    public List<WebElement> getSearchResults(){
        //xpath find all child li with div tag inside
//        return driver.findElements(By.xpath("//*[@id='center_column']/ul//child::li[div]//*[@class='product-container']//*[@class='left-block']/div/a[1]"));
        return driver.findElements((By.cssSelector("#center_column > ul > li > div > div.left-block > div.product-image-container > a:nth-child(1)" )));

    }

    public void typeTextInSearchField(String searchedText){
        searchInputField.sendKeys(searchedText);
    }

    public void clickOnSubmitSearch(){
        submitSearch.click();
    }

    public String getContentResultCounterText(){
        return resultCounterText.getText();
    }


}
