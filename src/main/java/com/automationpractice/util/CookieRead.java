package com.automationpractice.util;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class CookieRead {

    public static void getCookies(WebDriver driver) {
        // create file named Cookies to store Login Information
        File file = new File("C:\\mystore\\Cookies.data");
        try {
            // Delete old file if exists
            file.delete();
            file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWrite);

            // loop for getting the cookie information
            for (Cookie ck : driver.manage().getCookies()) {
                bufferedWriter.write((ck.getName() + ";" + ck.getValue() + ";" + ck.getDomain() + ";" + ck.getPath() + ";" + ck.getExpiry() + ";" + ck.isSecure()));
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            fileWrite.close();

        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
