package com.automationpractice.util;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;
import java.util.StringTokenizer;

public class CookieWrite {
    public static void writeCookie(WebDriver driver) {
        try {

            File file = new File("C:\\mystore\\Cookies.data");
            FileReader fileReader = new FileReader(file);
            BufferedReader Buffreader = new BufferedReader(fileReader);
            String strline;
            while ((strline = Buffreader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strline, ";");
//                while(token.hasMoreTokens()){
                String name = token.nextToken();
                String value = token.nextToken();
                String domain = token.nextToken();
                String path = token.nextToken();
                Date expiry = null;

                String val;
                if (!(val = token.nextToken()).equals("null")) {
                    expiry = new Date(2020, 8, 07, 20, 00);
                }
                Boolean isSecure = new Boolean(token.nextToken()).booleanValue();
                Cookie ck = new Cookie(name, value, domain, path, expiry, isSecure);

                driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
                driver.manage().addCookie(ck); // This will add the stored cookie to your current session
//                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //press F5 to refresh the page
        try {
            Robot r = new Robot();
            r.keyPress(116);
            r.keyRelease(116);

        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

}
